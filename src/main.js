const intake = document.getElementById('intake');
const sparkplug = document.getElementById('sparkplug');
const exhaust = document.getElementById('exhaust');
const piston = document.getElementById('piston');
const speedInput = document.getElementById('speedInput');
const phaseInput = document.getElementById('phaseInput');

document.addEventListener('DOMContentLoaded', () => {
  init((110 - speedInput.value) * 10, phaseInput.value);
});

function init(strokeTime, phase, prevInterval) {
  piston.style.animation = 'none';
  piston.offsetHeight;
  piston.style.animation = null;

  piston.style.animationDelay = -phase + 'ms';
  piston.style.animationDuration = strokeTime + 'ms';
  clearInterval(prevInterval);

  setTimeout(() => stroke(strokeTime, phase), phase);

  const interval = setInterval(() => stroke(strokeTime, phase), strokeTime * 2);

  speedInput.addEventListener('change', () => {
    init((110 - speedInput.value) * 10, phaseInput.value, interval);
  });

  phaseInput.addEventListener('change', () => {
    init((110 - speedInput.value) * 10, phaseInput.value, interval);
  });
}

function stroke(strokeTime, phase) {
  intakeCycle(0, strokeTime * 0.6);

  sparkplugCycle(strokeTime - 100 - phase, 200);

  exhaustCycle(strokeTime + strokeTime * 0.6 - phase, strokeTime * 0.5);
}

function intakeOpen() {
  intake.classList.remove('off');
}
function intakeClose() {
  intake.classList.add('off');
}
const intakeCycle = (openingTime, timeOpen) => {
  setTimeout(intakeOpen, openingTime);
  setTimeout(intakeClose, openingTime + timeOpen);
};

function exhaustOpen() {
  exhaust.classList.remove('off');
}
function exhaustClose() {
  exhaust.classList.add('off');
}
const exhaustCycle = (openingTime, timeOpen) => {
  setTimeout(exhaustOpen, openingTime);
  setTimeout(exhaustClose, openingTime + timeOpen);
};

function sparkplugOn() {
  sparkplug.classList.remove('off');
}
function sparkplugOff() {
  sparkplug.classList.add('off');
}
const sparkplugCycle = (onTime, timeOn) => {
  setTimeout(sparkplugOn, onTime);
  setTimeout(sparkplugOff, onTime + timeOn);
};
